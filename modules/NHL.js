// noinspection JSIgnoredPromiseFromCall,JSUnresolvedVariable

const axios = require('axios');
const fs = require("fs");
const moment = require('moment');
const {MessageEmbed} = require('discord.js');
const writeToConsole = require('./writeToConsole.js');

const nhlApiUrl = "https://api.nhle.com";
const nhlWebApiUrl = "https://api-web.nhle.com/v1";

const teamsFileLoc = './modules/NHL/teams.json'; // location of teams file, location based on top level bot location

let teams = [];

function buildGameFields(games, callback, guildId) {
    try {
        let fields = [];

        games.map(game => {
            const startHammerTime = moment(game.startTimeUTC).format('X');
            const gameDateString = `<t:${startHammerTime}:F> | <t:${startHammerTime}:R>`;

            let homeTeamStr = game.homeTeam.abbrev;
            let awayTeamStr = game.awayTeam.abbrev;

            if (guildId === '205358728872984576' || guildId === "209714039750787072") {
                homeTeamStr = getTeamEmoji(homeTeamStr);
                awayTeamStr = getTeamEmoji(awayTeamStr);
            }

            if (game.gameState === "FUT") {
                fields.push({
                    name: `${awayTeamStr} @ ${homeTeamStr}`,
                    value: `${gameDateString}
                    https://www.nhl.com${game.gameCenterLink}`,
                    inline: false
                })
            }
            else if (game.gameState === "LIVE") {
                fields.push({
                    name: `${awayTeamStr} @ ${homeTeamStr}`,
                    value: `Period ${game.periodDescriptor.number} | Score: ||${game.awayTeam.score} - ${game.homeTeam.score}||
                    https://www.nhl.com${game.gameCenterLink}`,
                    inline: false
                });
            } else if (game.gameState === "OFF" || game.gameState === "FINAL") {
                let winningTeam;

                winningTeam = game.awayTeam.score > game.homeTeam.score ? awayTeamStr : homeTeamStr

                fields.push({
                    name: `${awayTeamStr} @ ${homeTeamStr}`,
                    value: `Final | ||${game.awayTeam.score} - ${game.homeTeam.score} ${winningTeam} in ${game.periodDescriptor.periodType}||
                    https://www.nhl.com${game.gameCenterLink}`,
                    inline: false
                });
            } else {
                fields.push({
                    name: `${awayTeamStr} @ ${homeTeamStr}`,
                    value: `Game state: ${game.gameState}`,
                    inline: false
                });
            }
        });

        return callback(fields);
    } catch (err) {
        return callback(-1);
    }
}

const nhlInteractions = (interaction) => {
    try {
        if (interaction.options.getSubcommand() === 'team') {
            searchTeams(interaction.options.getString('team'), team => {
                if (team === -1) { // make sure we're getting a valid team
                    return interaction.reply({content: `Couldn't find a team with a search of ${interaction.options.getString('team')}...`});
                } else {
                    if (interaction.options.getString('compare')) { // comparing two teams
                        searchTeams(interaction.options.getString('compare'), team2 => { // get second team
                            if (team2 === -1) { // make sure team2 is a valid team
                                return interaction.reply({content: `Couldn't find a team with a search of ${interaction.options.getString('compare')}...`});
                            } else {
                                buildTeamFields(team.triCode, t1Fields => {
                                    if (t1Fields === -1) {
                                        writeToConsole('Failed to create t1Fields in compare block');
                                        return interaction.reply({content: "Something went wrong..."});
                                    } else {
                                        buildTeamFields(team2.triCode, t2Fields => {
                                            if (t1Fields === -1) {
                                                writeToConsole('Failed to create t2Fields in compare block');
                                                return interaction.reply({content: "Something went wrong..."});
                                            } else {
                                                const blankField = {
                                                    name: '\u200b',
                                                    value: '\u200b',
                                                    inline: true
                                                };
                                                const fields = [t1Fields[0], blankField, t2Fields[0],
                                                    t1Fields[1], blankField, t2Fields[1]];

                                                // noinspection JSCheckFunctionSignatures
                                                return interaction.reply({
                                                    content: `Current season stats for\n**${team.fullName}** vs. **${team2.fullName}**`,
                                                    embeds: [
                                                        new MessageEmbed()
                                                            .setColor('#010101')
                                                            .addFields(fields)
                                                    ]
                                                });
                                            }
                                        }, true);
                                    }
                                }, true);
                            }
                        })
                    } else { // only one team requested
                        buildTeamFields(team.triCode, fields => {
                            if (fields === -1) {
                                return interaction.reply({content: "Couldn't find a team with that search..."});
                            } else {
                                return interaction.reply({
                                    content: `Season stats for **${team.fullName}**`,
                                    embeds: [
                                        new MessageEmbed()
                                            .setColor('#010101')
                                            .addFields(fields)
                                    ]
                                });
                            }
                        });
                    }
                }
            })
        }
        else if (interaction.options.getSubcommand() === 'games') {
            if (interaction.options.getString('team') !== null) { // returns next 5 games for searched team
                searchTeams(interaction.options.getString('team'), team => {
                    getGamesByTeam(team, games => {
                        buildGameFields(games, fields => {
                            return interaction.reply({
                                content: `Games for ${team.fullName}`,
                                embeds: [
                                    new MessageEmbed()
                                        .setColor('#010101')
                                        .addFields(fields)
                                ]
                            });
                        }, interaction.guildId);
                    });
                });
            }
            else {
                // noinspection JSIgnoredPromiseFromCall
                getTodayGames(games => {
                    buildGameFields(games, fields => {
                        return interaction.reply({
                            content: `Games for ${moment(new Date()).format('YYYY-MM-DD')}`,
                            embeds: [
                                new MessageEmbed()
                                    .setColor('#010101')
                                    .addFields(fields)
                            ]
                        });
                    }, interaction.guildId);
                });
            }
        }
        }
    catch (e) {
        console.error(e);
        return interaction.reply({content: "Something went wrong, blame the NHL API"});
    }
}
exports.nhlInteractions = nhlInteractions;

// <editor-fold desc="teams">
function downloadNHLTeams(callback) {
    try {
        axios.get(`${nhlApiUrl}/stats/rest/en/team`)
            .then(res => {
                fs.writeFileSync(teamsFileLoc, JSON.stringify(res.data.data));
                console.log("NHL teams file updated!");
                callback(true);
            });
    } catch (err) {
        console.log("Something went wrong trying to download updated NHL teams file...")
        callback(false);
    }
}

// Checks to make sure nhl teams file exists. If not, download it. Also make sure it's not outdated
const checkNHLTeamsFile = (callback) => {
    // set how old the file should be before retrieving an updated version
    // variable is number of days, default 30 for teams file
    const teamsFileMaxAge = 30;

    if (fs.existsSync(teamsFileLoc)) { // check file exists
        console.log('NHL teams file found');
        const currentDate = new Date();
        const teamsFileCreatedDate = new Date(fs.statSync(teamsFileLoc).mtime);

        const teamsFileAge = (currentDate - teamsFileCreatedDate)/86400000; // age of teams file, in days

        if (teamsFileAge > teamsFileMaxAge) {
            console.log('NHL teams file older than max age, downloading updated version now...')
            downloadNHLTeams(success => {return callback(success)});
        } else {
            return callback(true);
        }
    } else {
        console.log('NHL teams file not found, downloading now...')
        downloadNHLTeams(success => {return callback(success)});
    }
}
exports.checkNHLTeamsFile = checkNHLTeamsFile;

const retrieveNHLTeams = () => {
    // noinspection JSCheckFunctionSignatures,JSUnresolvedVariable
    teams = JSON.parse(fs.readFileSync(teamsFileLoc));
}
exports.retrieveNHLTeams = retrieveNHLTeams;

function buildTeamFields(teamTriCode, callback, inline = false) {
    let fields = [];
    let isError = false;

    axios.get(`${nhlWebApiUrl}/standings/now`)
        .catch(_ => isError = true)
        .then(res => {
            if (isError) return callback(-1);
            else {
                getTeamStatsFromApiRes(res.data.standings, teamTriCode, teamStats => {
                    fields.push({
                        name: `Season Results`,
                        value: `${teamStats.wins}-${teamStats.losses}-${teamStats.otLosses} | ${teamStats.points} points`,
                        inline: inline
                    });

                    fields.push({
                        name: `Goals`,
                        value: `GFA ${roundNumTo2(teamStats.goalFor / teamStats.gamesPlayed)} | Total GF: ${teamStats.goalFor}
                        GAA ${roundNumTo2(teamStats.goalAgainst / teamStats.gamesPlayed)} | Total GA: ${teamStats.goalAgainst}`,
                        inline: inline
                    });

                    callback(fields);
                });
            }
        });
}
// </editor-fold desc="teams">

// <editor-fold desc="games">
function getTodayGames(callback) {
    const todayStr = moment(new Date()).format("YYYY-MM-DD");

    let isError = false;

    axios.get(`${nhlWebApiUrl}/schedule/now`)
        .catch(_ => isError = true)
        .then(res => {
            if (!isError) {
                const gameWeek = res.data.gameWeek;

                callback(gameWeek.filter(day => {
                    return day.date === todayStr;
                })[0].games);
            }
            else return callback(-1);
        });
}

function searchTeams(search, callback) {
    const selectedTeam = teams.filter(team => {
        // noinspection JSUnresolvedVariable
        return team.fullName.toLowerCase().includes(search) ||
            team.rawTricode.toLowerCase().includes(search) ||
            team.triCode.toLowerCase().includes(search)
    })[0];

    if (selectedTeam === undefined) {
        return callback(-1)
    }
    else {
        return callback(selectedTeam);
    }
}
// </editor-fold desc="games">
function getTeamStatsFromApiRes(data, teamTriCode, callback) {
    callback(data.filter(team => {
        return team.teamAbbrev.default.toLowerCase() === teamTriCode.toLowerCase();
    })[0]);
}

function roundNumTo2(number) {
    return Math.round(number * 100) / 100;
}

function getGamesByTeam(team, callback) {
    let isError = false;
    axios.get(`${nhlWebApiUrl}/club-schedule-season/${team.triCode}/now`)
        .catch(_ => isError = true)
        .then(res => {
            if (isError) return callback(-1);
            else {
                let games = res.data.games;

                let nextGameId = games.findIndex(game => {
                    return game.gameDate > moment(new Date()).format('YYYY-MM-DD')
                })

                if (nextGameId + 3 > games.length) {
                    nextGameId = games.length - 3;
                }

                let shownGames = [];

                for (let i = nextGameId - 3; i < nextGameId + 3; i++) {
                    shownGames.push(games[i]);
                }

                callback(shownGames);
            }
        });
}

function getTeamEmoji(teamAbbrev) {
    switch (teamAbbrev) {
        case "ANA": return `${teamAbbrev} <:nhl_ANA:1175150742030516264>`;
        case "CAR": return `${teamAbbrev} <:nhl_CAR:1175150740822568980>`;
        case "SJS": return `${teamAbbrev} <:nhl_SJS:1175150738511503432>`;
        case "SEA": return `${teamAbbrev} <:nhl_SEA:1175150737207066755>`;
        case "WSH": return `${teamAbbrev} <:nhl_WSH:1175150735697137795>`;
        case "VGK": return `${teamAbbrev} <:nhl_VGK:1175150732878548993>`;
        default: return teamAbbrev;
    }
}