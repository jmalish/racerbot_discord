const fs = require('fs');
const sendMessage = require('./sendMessage.js');
const writeToConsole = require('./writeToConsole.js');

const checkQuotesDir = (quotesDir) => {
    if (fs.existsSync(quotesDir)) { // check for quotes directory and
        console.log('Quotes directory found!');
    } else { // create it if it doesn't exist
        console.log('Quotes directory not found, creating it now');
        fs.mkdirSync(quotesDir);
    }
}
exports.checkQuotesDir = checkQuotesDir;

const quotesMessage = (client, message, quotesDir) => {
    let channel = message.channel;
    let messageContentsLower = message.content.toLowerCase();

    // check for a quotes file for the requested server and create it if it doesn't exist
    // returns 1 if file exists, 0 if it was created, and -1 if error
    function serverQuotesFileExists(serverId, exists) {
        let serverQuotesFile = quotesDir + serverId + '.json';

        if (fs.existsSync(serverQuotesFile)) {
            // console.log(`Quotes file for serverId ${serverId} found!`);
            exists(1);
        } else {
            console.log(`Quotes file for serverId ${serverId} not found, creating it now`);
            try {
                fs.writeFileSync(serverQuotesFile, `{"serverId": "${serverId}", "quotes": []}`);
                exists(0);
            } catch (e) {
                console.error(e);
                exists(-1);
            }
        }
    }

    function getAllQuotes(serverId, callback) {
        serverQuotesFileExists(serverId, exists => {
            if (exists === 1) {
                fs.readFile(quotesDir + serverId + '.json', 'utf8',  (err, data) =>
                    callback(JSON.parse(data).quotes));
            } else {
                callback(-1);
            }
        });
    }

    function getQuoteById(serverId, requestedId, context, callback) {
        getAllQuotes(serverId, quotes => {
            if (quotes === -1) {
                callback("There are no quotes for this server, add some with `!grab` and `!quote add`!");
            }

            let quote = quotes.find(quote => {
                return quote.quoteId === requestedId;
            });

            if (quote) {
                if (!context) {
                    buildQuoteForMessage(quote, message => {
                        callback(message);
                    });
                } else {
                    callback(quote);
                }
            } else {
                callback("I couldn't find a quote #" + requestedId);
            }
        })
    }

    function addNewQuote(newQuote, callback) { // TODO convert pings to plain text names when adding quote
        let serverId = newQuote.serverId;
        let serverQuotesFile = quotesDir + serverId + '.json';

        serverQuotesFileExists(serverId, exists => {
            if (exists !== -1) {
                try {
                    fs.readFile(serverQuotesFile, 'utf8',  (err, data) => {
                        let json = JSON.parse(data);

                        if (json.quotes.length === 0) {
                            newQuote.quoteId = 1;
                        } else {
                            newQuote.quoteId = json.quotes[json.quotes.length - 1].quoteId + 1;
                        }

                        json.quotes.push(newQuote);

                        // noinspection JSCheckFunctionSignatures
                        fs.writeFile(serverQuotesFile, JSON.stringify(json), 'utf-8', function (err) {
                            if (err) callback(false);
                            callback(newQuote.quoteId);
                        });
                    })
                } catch (e) {
                    console.error(e);
                }
            }
        });
    }

    function getRandomQuote(_serverId, callback) {
        getAllQuotes(_serverId, quotes => {
            if (quotes === -1 || quotes.length === 0) {
                callback(undefined);
            } else {
                callback(quotes[getRandomInt(0, quotes.length)]);
            }
        });
    }

    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
    }

    function deleteQuote(serverId, requestedId, callback) {
        let serverQuotesFile = quotesDir + serverId + '.json';

        serverQuotesFileExists(serverId, exists => {
            if (exists !== -1) {
                fs.readFile(serverQuotesFile, function (err, data) {
                    // noinspection JSCheckFunctionSignatures
                    let json = JSON.parse(data);

                    let findQuote = json.quotes.find(quote => {
                        return quote.quoteId === requestedId;
                    });

                    if (findQuote) {
                        json.quotes = json.quotes.filter(quote => {
                            return quote.quoteId !== requestedId;
                        });

                        fs.writeFile(serverQuotesFile, JSON.stringify(json), 'utf-8', function (err) {
                            if (err) callback(false);
                            callback(true);
                        });
                    } else {
                        callback(false);
                    }
                });
            }
        });
    }

    function buildQuoteForMessage(quote, callback) {
        let containsPing = quote.quoteText.match(/<@!?([0-9]+)>/);
        while (containsPing) {
            try {
                quote.quoteText = quote.quoteText.replace(containsPing[0], client.users.get(containsPing[1]).username);
            } catch (e) {
                quote.quoteText = quote.quoteText.replace(containsPing[0], "~~user not found~~");
            }

            containsPing = quote.quoteText.match(/<@([0-9]+)>/);
        }

        callback(`Quote #${quote.quoteId} by ${quote.author}:` + '\n'
            + `>>> ${quote.quoteText}`);
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    if (messageContentsLower.indexOf('!quote') > -1) {
        let quoteVars = message.content.split(' ');

        if (quoteVars.length === 1) { // any quote is requested
            getRandomQuote(message.guild.id, quote => {
                if (quote === undefined) {
                    sendMessage(channel, "No quotes found for this server! Add some with `!quote add` or `!grab <@username>`");
                } else {
                    buildQuoteForMessage(quote, message => {
                        sendMessage(channel, message);
                    });
                }
            })
        }
        else if (quoteVars[1].match(/[0-9]+/)) { // specific quote was requested
            try {
                let requestedId = Number(quoteVars[1]);

                if (quoteVars[2] === 'context') {
                    getQuoteById(message.guild.id, requestedId, true, quote => {
                        let discordUrl = `https://discordapp.com/channels/${quote.serverId}/${quote.channelId}/${quote.messageId}`;

                        sendMessage(channel, discordUrl);
                    });
                } else {
                    getQuoteById(message.guild.id, requestedId, false, quote => {
                        sendMessage(channel, quote);
                    });
                }
            } catch (err) {
                // I don't see this catch ever getting hit, but just to be safe
                console.log(err);
                sendMessage(channel, "I think something went wrong?", _ => {});
            }
        }
        else if (quoteVars[1] === 'help') {
            sendMessage(channel, '`!quote add <new quote> -user`', _ => {});
        }
        else if (quoteVars[1] === 'add') {
            let quoteRegex = message.content.match(/"(.+)" -(.+)/);

            if (quoteRegex != null && quoteRegex[1] && quoteRegex[2]) {
                let newQuote = new Quote(quoteRegex[1], quoteRegex[2], message.guild.id, channel.id, message.id);

                addNewQuote(newQuote, success => {
                    if (success) {
                        sendMessage(channel, 'Quote added as #' + success + '!', _ => {});
                    }
                });
            } else {
                sendMessage(channel, '`!quote add "<new quote>" -<author>`', _ => {});
            }
        }
        else if (quoteVars[1] === 'delete') {
            if (quoteVars.length === 2) {
                sendMessage(channel, 'Which quote am I deleting? `!quote delete <id>`', _ => {});
            } else {
                let deleteId = Number(quoteVars[2].match(/[0-9]+/)[0]);

                deleteQuote(message.guild.id, deleteId, success => {
                    if (success) {
                        sendMessage(channel, "I didn't think it was worth keeping either...", _ => {});
                    } else {
                        sendMessage(channel, "Either something went wrong or that's not a valid quote Id", _ => {});
                    }
                });
            }
        }
    }
    // Grab command (!grab <username, must be in @username discord format)
    else if (messageContentsLower.indexOf('!grab') > -1) { // quote grab requested
        let grabVars = message.content.split(' ');

        if (grabVars.length === 1) { // wasn't given any variables, so warn
            sendMessage(channel, 'Who am I grabbing? `!grab <username>`');
            writeToConsole("Grab command received with no grabbee given.");
        } else { // we got more than one var, so continue
            let userIdToGrab = grabVars[1].match(/<@!?([0-9]+)>/); // get requested grabbee

            // the ones and zeros here are used to figure out how many messages we need to grab by adding and subtracting later
            if (userIdToGrab != null) { // make sure variable exists
                let messagesToGrab = 1;
                let selfGrab = 0;
                userIdToGrab = userIdToGrab[1];

                if (userIdToGrab === message.author.id) { // if user is grabbing themselves
                    selfGrab = 1;
                }

                if (grabVars[2]) { // if there is a third var we need to get number of messages
                    if (grabVars[2].match(/[0-9]+/)) {
                        messagesToGrab = Number(grabVars[2].match(/[0-9]+/));
                    }
                }

                if (messagesToGrab > 5) { // set upper limit on messages to grab
                    sendMessage(channel, "Whoa now, let's tone that down to 5 or less messages...");
                } else {
                    let userMessages = [];

                    // noinspection JSUnresolvedFunction
                    channel.messages.fetch({limit: 50})
                        .then((msgCollection) => { // Resolve promise
                        msgCollection.forEach((msg) => { // forEach on message collection
                            // noinspection JSIncompatibleTypesComparison
                            if (msg.author.id === userIdToGrab) {
                                userMessages.push(msg)
                            } // put all messages from user into userMessages array
                        });
                    }).then(_ => {
                        let quoteContent = ''; // create initial quoteContent variable so we can build on it

                        let linkedMessage; // used so we can link back to quote later
                        for (let i = messagesToGrab - 1; i >= 0; i--) {
                            quoteContent += userMessages[i + selfGrab].content;
                            if (i !== 0) {
                                quoteContent += '\n';
                            } else {
                                linkedMessage = userMessages[i + selfGrab];
                            }
                        }

                        client.users.fetch(userIdToGrab)
                            .then(user => {
                                addNewQuote(new Quote(quoteContent, user.username, linkedMessage.guild.id, linkedMessage.channel.id, linkedMessage.id),
                                    success => {
                                        sendMessage(channel, 'Quote added as #' + success + '!');
                                        writeToConsole('Quote added as #' + success + '!');
                                    });
                            });
                    });
                }
            } else {
                sendMessage(channel, "What, you think I'm that smart? Try `!grab <@username>`");
                writeToConsole("Unable to grab, unable to resolve requested username")
            }
        }
    }
}
exports.quotesMessage = quotesMessage;

class Quote {
    constructor(_quoteText, _author, _serverId, _channelId, _messageId) {
        this.quoteText = _quoteText;
        this.author = _author;
        this.serverId = _serverId;
        this.channelId = _channelId;
        this.messageId = _messageId;
        this.timeCreated = Date.now();
        this.quoteId = -1;
    }
}