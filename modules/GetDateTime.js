module.exports = function (callback) { // gets date and time for logging purposes
    let currentTimeMonth = new Date().getMonth() + 1;
    let currentTimeDay = new Date().getDate();

    let currentTimeHour = new Date().getHours();
    let currentTimeMin = new Date().getMinutes();
    if (currentTimeMin < 10) {
        currentTimeMin = 0 + currentTimeMin.toString();
    }

    callback(currentTimeMonth + '/' + currentTimeDay + ' ' + currentTimeHour + ':' + currentTimeMin);
}