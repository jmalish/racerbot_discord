const sendMessage = require('./SendMessage');

module.exports = function (client, message) {
	let messageContentsLower = message.content.toLowerCase();
    
    // Command to say anything on a given server (usage - !say <channelId> <message>
    if (messageContentsLower.indexOf('!say') > -1) {
        try {
            let grabVars = message.content.split(' ');

            if (/[0-9]{18}/g.exec(grabVars[1])) { // check we received a valid channel id
                let messageToSend = message.content.split(grabVars[1] + ' ')[1];

                // client.channels.cache.get(grabVars[1]).send(messageToSend);
                sendMessage(client.channels.cache.get(grabVars[1]), messageToSend);
            } else {
                sendMessage(message.channel, "That's not a valid Channel ID! `!say <channel ID> <message>`");
            }
        } catch (e) {
            console.log(e);
        }
    }
}