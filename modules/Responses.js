const sendMessage = require('./SendMessage');

function niceReaction(message) {
    message.react("🇳")
        .then(_ => message.react("🇮")
            .then(_ => message.react("🇨")
                .then(_ => message.react("🇪"))));
}

const messageResponses = (client, message) => {
    const channel = message.channel;
    const messageContentsLower = message.content.toLowerCase();

    // unfortunately, this bot is a NARC
    if (messageContentsLower.includes("weed") || messageContentsLower.includes("420")) {
        message.react("🚓").then();
    }

    // it's also a child
    if (messageContentsLower.includes("69") || messageContentsLower.includes("sixty nine")) {
        // we don't want to nice a message if the 69 is in a userId
        // execute regex to check if the message contains a userId
        let messageContainsUserCheck = /(<@!?[0-9]{18}>)/gi.exec(messageContentsLower);

        if (!messageContainsUserCheck) { // if it does do additional check
            if (!messageContentsLower.includes("http") && !messageContentsLower.includes(".com")) {
                niceReaction(message);
            }
        }
    }

    // if someone says "wat" or "what" in the channel, say it louder for them
    if (messageContentsLower === 'wat' || messageContentsLower === 'what' || messageContentsLower === 'wut') {
        message.channel.messages.fetch({limit: 2}) // Fetch last 2 messages.
            .then((msgCollection) => { // Resolve promise
                let messages = []; // stores messages

                msgCollection.forEach((msg) => { // forEach on message collection
                    messages.push(msg); // add each message to our local collection
                });

                if (messages[1].author.id === client.user.id && (messages[1].content.indexOf('He said') > -1 || messages[1].content.indexOf('You heard me') > -1)) {
                    sendMessage(channel, `You heard me...`)
                } else {
                    let messageToSend = ''; // create message variable
                    let isLastCap = false; // bool used to check if we need to add asterisks or not

                    messages[1].content.split('').forEach(char => { // take the second message in array (first is the wat request), and split into an array of chars
                        if (char.match(`[A-Z]`)) { // char is uppercase
                            if (!isLastCap) { // check if this is the first cap character
                                messageToSend += '**'; // if it is, add the two asterisks
                            }
                            messageToSend += char; // add the character to the message
                            isLastCap = true; // set bool to true

                        } else { // lowercase or non character (comma, question mark, etc)
                            if (isLastCap) { // check if last char was a capital
                                messageToSend += '**'; // if it was, add asterisks to END the boldening
                                isLastCap = false; // reset bool
                            }
                            messageToSend += char.toUpperCase(); // set the character to upper case and add to message
                        }
                    });

                    if (isLastCap) { // reached the end of the message, check if we need closing asterisks
                        messageToSend += '**'; // add them if so
                    }

                    sendMessage(channel, `He said "${messageToSend}"`); // send the message to discord
                }
            });
    }

    if (messageContentsLower === "bye") {
        sendMessage(channel, "https://cdn.discordapp.com/attachments/618211204430561290/1108933961041842227/iu.png");
    }
}
exports.messageResponses = messageResponses;