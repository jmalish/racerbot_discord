const {Client, Intents} = require('discord.js');
require('dotenv').config(); // load config from .env file

const client = new Client({intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES]}); // the actual bot and discord API
const quotesDir = './serverQuotes/'; // Directory that contains a json file for each server

// import modules
const writeToConsole = require('./modules/writeToConsole.js');
const {checkNHLTeamsFile, nhlInteractions, retrieveNHLTeams} = require('./modules/NHL');
const {checkQuotesDir, quotesMessage} = require('./modules/Quotes');
const {messageResponses} = require('./modules/Responses');

// called when the client connects and is "ready"
client.once('ready', () => { // client connected to discord
    writeToConsole(`Logged in as ${client.user.tag}`);
});

startup(); // run startup function

// called whenever a new message is received in a channel the bot is part of
client.on('messageCreate', message => {
    messageResponses(client, message);

    if (message.author.bot) return; // anything beyond this point will ignore a message if it originates from a bot on the server

    // make sure message starts with our command indicator
    if (message.content.indexOf('!') !== 0) {
        return 0;
    } else {
        writeToConsole(`${message.content} [requested by ${message.author.username}]`);
        require('./modules/say')(client, message);
        quotesMessage(client, message, quotesDir);
    }
});

// called when a user uses a slash command
client.on('interactionCreate', async interaction => {
    if (!interaction.isCommand()) return;
    const {commandName} = interaction;


    if (commandName === 'nhl') {
        nhlInteractions(interaction);
    }
})


// called when the bot runs into an error
client.on('error', err => {
    console.error(`Error: ${err.message}`);
});

if (process.env.DEV === "true") {
    console.log("~~~~~~~~~~~~ Dev ~~~~~~~~~~~~");
    client.login(process.env.TOKEN2).then(); // set token and login
} else {
    client.login(process.env.TOKEN).then(); // set token and login
}


function startup() {
    checkQuotesDir(quotesDir);
    checkNHLTeamsFile(_ => {retrieveNHLTeams()});
}