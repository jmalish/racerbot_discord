# Racerbot - A discord bot
Racerbot was created to have better quotes compared to other bots, also because I was bored.
I imagine more stuff will be added to the bot later, maybe, probably.

### Setup
- Run `npm install` to install dependencies
- Get your bots token from the discord dev site and create a .env file using the format as sample.env
- Run `npm start` to start (this requires nodemon)

### Commands - All commands are preceded by a !
- grab (Grabs the last message from the selected user `!grab @racer0940`
- quote (just `!quote` will return a random quote
    - help (lists the quote commands)
    - add (adds a new quote `!quote "<quote text>" -<author>`
    - delete (deletes the requested quote `!quote delete <quoteID>`