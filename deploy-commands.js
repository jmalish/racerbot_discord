const {SlashCommandBuilder, SlashCommandSubcommandBuilder} = require('@discordjs/builders');
const {REST} = require('@discordjs/rest');
const {Routes} = require('discord-api-types/v9');
require('dotenv').config();

const commands = [
    new SlashCommandBuilder()
        .setName('nhl')
        .setDescription('Get info about NHL teams and games.')
        .addSubcommand(subCommand => {
            return subCommand
                .setName('team')
                .setDescription('Get information about a team.')
                .addStringOption(option => {
                    return option
                        .setName('team')
                        .setDescription('team name')
                        .setRequired(true)
                })
                .addStringOption(option => {
                    return option
                        .setName('compare')
                        .setDescription('team2 name')
                        .setRequired(false);
                })
        })
        .addSubcommand(subCommand => {
            return subCommand
                .setName('games')
                .setDescription('Get the schedule and scores of games happening today.')
                .addStringOption(option => {
                    return option
                        .setName('team')
                        .setDescription('Get games for a given team.')
                        .setRequired(false)
                })
                // .addStringOption(option => {
                //     return option
                //         .setName('versus')
                //         .setDescription('Returns the next 4 games between the two requested teams. Usage: <team1> <team2>')
                //         .setRequired(false)
                // })
                // .addStringOption(option => {
                //     return option
                //         .setName('date')
                //         .setDescription('Get games from a given date. Usage: <YYYY-MM-DD> | Also accepts `yesterday` and `tomorrow`')
                //         .setRequired(false)
                // })
        })
].map(command => command.toJSON());

// noinspection JSCheckFunctionSignatures
const rest = new REST({version: 9}).setToken(process.env.TOKEN);

// DELETES COMMANDS
console.log("Deploy disabled, uncomment lines as needed");
// rest.put(Routes.applicationCommands(process.env.CLIENT_ID), { body: [] })
//     .then(() => console.log('Successfully deleted all application commands.'))
//     .catch(console.error);
//
// rest.put(Routes.applicationGuildCommands(process.env.CLIENT_ID, process.env.SERVER_ID), { body: [] })
//     .then(() => console.log('Successfully deleted all guild commands.'))
//     .catch(console.error);
//
// rest.put(Routes.applicationCommands(process.env.CLIENT_ID), {body: commands})
//     .then(() => {console.log(`Commands registered`)})
//     .catch(console.error);